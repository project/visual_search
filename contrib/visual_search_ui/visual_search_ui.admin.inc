<?php

function visual_search_ui_admin_settings($form_state) {
  $form = array();
  $form['visual_search_ui']['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsed' => false,
    '#collapsible' => true,
  );
  $tolerance_options = drupal_map_assoc(range(1,25, 1));
  $form['visual_search_ui']['settings']['visual_search_tolerance'] = array(
    '#type' => 'select',
    '#title' => t('Visual Search Tolerance'),
    '#options' => $tolerance_options,
    '#default_value' => variable_get('visual_search_tolerance', 25),
    '#description' => t('This option allows site administrators to configure how sensitive the search is in terms of color saturation within an '.
      'image.  This refers to the percentage that a specific color appears within the image.  Set this number to a higher value for more specific '.
      'results, and lower for more generic results.  Depending on the number of indexed images, a lower tolerance may produce better results.'),
  );
  $distance_options = drupal_map_assoc(range(0,200,25));
  $form['visual_search_ui']['settings']['visual_search_distance'] = array(
    '#type' => 'select',
    '#title' => t('Color Range'),
    '#options' => $distance_options,
    '#default_value' => variable_get('visual_search_distance', 5),
    '#description' => t('This option allows site administrators to configure how large of a range of colors should be considered within the search.  '.
      'Larger values in this configuration will produce larger results, as more colors will be matched against the selected color.  However, this '.
      'may also increase the load on the database, depending on the number of indexed images.  To narrow the result set, configure this option to '.
      'a lower value.'), 
  );
  $form['visual_search_ui']['settings']['visual_search_results_per_page'] = array(
    '#type' => 'select',
    '#title' => t('Results Per Page'),
    '#options' => drupal_map_assoc(range(20, 100, 20)),
    '#default_value' => variable_get('visual_search_results_per_page', 20),
    '#description' => t('Select the number of results to display per page.'),
  );
  
  $form['visual_search_ui']['color_pickers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visual Search Color Pickers'),
    '#collapsed' => false,
    '#collapsible' => true,
  );
  static $colorpickers;
  $names = array();
  if (is_null($colorpickers)) {
    foreach (module_list() as $module) {
      if (module_hook($module, 'colorpicker')) {
        $names = module_invoke($module, 'colorpicker', 'info');
        foreach($names as $delta => $info) {
          $colorpickers[$module.':'.$delta] = $info['name'];
        }
      }
    }
  }
  $default = variable_get('active_color_picker', 0);
  $form['visual_search_ui']['color_pickers']['active_color_picker'] = array(
    '#type' => 'radios',
    '#title' => t('Choose which color picker to use for Visual Searching'),
    //'#options' => $colorpickers,
    '#default_value' => variable_get('active_color_picker', 0)
  );
  $count = 0;
  //die('Names:<br><pre>'.print_r($names,true).'</pre>');
  foreach ($colorpickers as $delta => $title) {
    list($module,$index) = explode(':', $delta);
    $form['visual_search_ui']['color_pickers']['active_color_picker'][$count] = array(
      '#type' => 'radio',
      '#return_value' => $delta,
      '#suffix' => '<div class="description">'.$names[$index]['description'].'</div>',
      '#parents' => array('active_color_picker'),
      '#title' => $title,
      '#default_value' => $default == $delta ? $delta : 0,
    );
    $count++;
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Settings',
  );
  return $form;
  //return system_settings_form($form);
}

function visual_search_ui_admin_settings_submit($form, $form_state) {
  $current_colorpicker = variable_get('active_color_picker', 0);
  if($current_colorpicker != $form_state['values']['active_color_picker']) {
    variable_set('previous_color_picker', $current_colorpicker);
  }
  foreach($form_state['values'] as $variable => $value) {
    variable_set($variable, $value);
  }
  //die('Form_state:<br><pre>'.print_r($form_state,true).'</pre>');
}