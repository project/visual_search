$('#edit-selected-color').ColorPicker({
  onSubmit: function(hsb, hex, rgb, el) {
    $(el).val('#' + hex);
    $(el).ColorPickerHide();
    $(el).css('background-color', '#' + hex);
  },
  onShow: function (colpkr) {
    $(colpkr).fadeIn(1000);
    return false;
  },
  onBeforeShow: function () {
    $(this).ColorPickerSetColor(this.value);
    $(this).css('background-color', this.value);
  }
})
.bind('keyup', function(){
  $(this).ColorPickerSetColor(this.value);
})
.click();

