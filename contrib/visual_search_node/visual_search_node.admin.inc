<?php

/**
 * Menu callback function that creates an admin configuration form.
 * 
 * @return
 *   FAPI array representing the configuration form
 */
function visual_search_node_admin_content_types() {
  $types = node_get_types();
  $all_node_type_options = array();
  foreach ($types as $type) {
    $all_node_type_options[$type->type] = t($type->name);
  }
  
  $form = array();
  $form['visual_search_node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visual Search Content Types'),
    '#collapsed' => false,
    '#collapsible' => false
  );
  
  $form['visual_search_node']['visual_search_content_types'] = array(
    '#type' => 'select',
    '#multiple' => true,
    '#options' => $all_node_type_options,
    '#default_value' => variable_get('visual_search_content_types', array()),
    '#required' => false,
    '#title' => 'Content Types',
  );
  return system_settings_form($form);
}

function visual_search_node_process() {
	$form = array();
	$form['visual_search_node']['process'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Process Images'),
	  '#collapsed' => false,
	  '#collapsible' => false,
	);
	$form['visual_search_node']['process']['visual_search_nodes_per_batch'] = array(
	  '#type' => 'select',
	  '#title' => t('Nodes per batch'),
	  '#options' => drupal_map_assoc(range(100, 500,100)),
	  '#default_option' => variable_get('visual_search_nodes_per_batch', 100),
	  '#description' => t('Select the number of nodes to process per batch run.'),
	);
	$form['visual_search_node']['process']['rebuild'] = array(
	  '#type' => 'checkbox',
	  '#title' => t('Purge all image data?'),
	  '#description' => t('If selected, this option will purge all parsed image data from the database.  '),
	);
	$form['submit'] = array(
	  '#type' => 'submit',
	  '#value' => t('Process Images'),
	);
	return $form;
}
/**
 * Menu callback function for starting the batch process to index all image files
 */
function visual_search_node_process_submit(&$form, $form_state) {
  // define batch array structure
  $batch = array(
    'title' => t('Indexing image files'),
    'operations' => array(
      array('_visual_search_node_batch_read',array($form_state['values']['rebuild'])),
    ),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Visual Search node processing has encountered an error.'),
    'file' => drupal_get_path('module', 'visual_search_node') . '/visual_search_node.admin.inc',
  );
  // set batch
  batch_set($batch);

  // process batch
  //batch_process('admin/settings/visual-search/content-types');
  batch_process();
}

/**
 * TODO: convert this to a batch api process and add support for imagefield
 */
function _visual_search_node_batch_read($rebuild=true, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = db_result(db_query('SELECT COUNT(DISTINCT nid) FROM {node}'));
  }
  
	if($rebuild) {
  	db_query('DELETE FROM {visual_search_imagedata}');
  	//drupal_set_message('All Visual Search image data has been wiped.');
  }
  
  $limit = variable_get('visual_search_nodes_per_batch', 100);

  // With each pass through the callback, retrieve the next group of nids.
  $result = db_query_range("SELECT nid FROM {node} WHERE nid > %d ORDER BY nid ASC", $context['sandbox']['current_node'], 0, $limit);
	$types = variable_get('visual_search_content_types', array());
  if(! empty($types)) {
    $result = db_query_range('SELECT nid FROM {node} WHERE type IN ('.db_placeholders($types, 'varchar').') AND nid > %d ORDER BY nid ASC', $types, $context['sandbox']['current_node'], 0, $limit);
    while($row = db_fetch_array($result)) {
      $node = node_load($row['nid']);
      
      if($node) {
        // Store some result for post-processing in the finished callback.
		    $context['results'][] = check_plain($node->title);
		
		    // Update our progress information.
		    $context['sandbox']['progress']++;
		    $context['sandbox']['current_node'] = $node->nid;
		    $context['message'] = t('Now processing %node', array('%node' => $node->title));
    
      	// Handle nodes with uploaded files
      	if(! empty($node->files)) {
          foreach($node->files as $file) {
            // If the file is an image, parse it
            if(ereg('^(image/)', $file['filemime'])) {
              if($file['new']) {                  
              	visual_search_api_process('insert', $file);
              }
              else {
                visual_search_api_process('update', $file);
              }
            }           
          }         
        }
        
        // Handle nodes with imagefields
			  if(module_exists('imagefield')) {
			    // Retrieve all of the information about this node's fields 
			    $fields = content_fields(null, $node->type);
			    if(!empty($fields)) {
            foreach($fields as $field_name => $field) {
			        // Only look at image fields that are active
			        if(($field['type'] == 'filefield') && ($field['type_name'] == 'image') && ($field['widget_active'] > 0)) {
			          foreach($node->$field_name as $index => $file) {
			            if(ereg('^(image/)', $file['filemime'])) {
                    // Does the imagedata already exist?
				            if($rebuild) {
				              visual_search_api_process('insert', $file);	
				            }
				            else {
				              visual_search_api_process('update', $file);
				            }
			            }
			          }    
			        }
			      }
			    }
			  }
      }
      // Inform the batch engine that we are not finished,
	    // and provide an estimation of the completion level we reached.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }
}